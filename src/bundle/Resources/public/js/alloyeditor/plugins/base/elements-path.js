const UpdateElementsPathCommand = {
    elements: [],
    editor: null,
    itemTemplate: new CKEDITOR.template('<a href="#">{label}</a>'),
    cssClasses: {
        customTagWrapper: 'cke_widget_ezcustomtag',
        embedWrapper: 'cke_widget_ezembed',
        embedImage: 'ez-embed-type-image',
        customTagContentVisible: 'ez-custom-tag--content-visible'
    },
    attributes: {
        eZName: 'data-ezname',
        eZElement: 'data-ezelement',
        eZType: 'data-eztype',
        widgetName: 'data-widget'
    },
    skipElements: [
        'thead',
        'tbody'
    ],
    exec: function(editor, options) {
        this.editor = editor;
        this.editor.elementsPathContainer.setHtml('');

        // Reverse path and skip first item, which is editor container
        this.elements = options.path.reverse().slice(1);

        // Get output items and build path for them
        this.getOutputPathElements(options.dialogs).forEach(function (outputItem) {
            this.editor.elementsPathContainer.append(outputItem.pathItem);
        }, this);
    },
    getElementLabel: function(tag) {
        const key = 'elements.' + tag + '.label';
        const label = Translator.trans(key, {}, 'custom_attributes');

        // return tag name if there is no translation
        return label !== key ? label : tag;
    },
    getOutputPathElements: function(dialogs) {
        let outputPathElements = [];
        const length = this.elements.length;

        for (let i = 0; i < length; i++) {
            let element = this.elements[i];
            if (element !== null && this.skipElements.indexOf(element.getName()) === -1) {
                const outputPathElement = this.getOutputPathItem(element, i, dialogs);
                if (typeof outputPathElement.pathItem !== 'undefined') {
                    outputPathElements.push(outputPathElement);
                }
            }
        }

        return outputPathElements;
    },
    getOutputPathItem: function(element, index, dialogs) {
        let outputItem = {};
        let elementName = element.getName();
        let label = this.getElementLabel(elementName);

        if (this.isBlockCustomTag(element) || this.isInlineCustomTag(element)) {
            outputItem.originalElement = element;
            label = this.getCustomTagPathLabel(element);
            this.fixElementsForCustomTag(element, index);
        } else if (this.isEmbedImage(element)) {
            label = this.getElementLabel('image');
        } else if (this.isEmbed(element)) {
            label = this.getElementLabel('embed');
        } else if (this.isBlockCustomStyle(element) || this.isInlineCustomStyle(element)) {
            label = this.getCustomStylePathLabel(element);
        }

        outputItem.pathItem = this.getPathItem(element, label, dialogs);
        return outputItem;
    },
    isCustomTag: function(element) {
        return element.hasClass(this.cssClasses.customTagWrapper);
    },
    isBlockCustomTag: function(element) {
        if (this.isCustomTag(element) === false) {
            return false;
        }

        let list = element.find('[' + this.attributes.eZElement + ']');
        if (list.count() === 0) {
            return false;
        }

        return list.getItem(0).getAttribute(this.attributes.widgetName) === 'ezcustomtag';
    },
    isInlineCustomTag: function(element) {
        if (this.isCustomTag(element) === false) {
            return false;
        }

        let list = element.find('[' + this.attributes.eZElement + ']');
        if (list.count() === 0) {
            return false;
        }

        return list.getItem(0).getAttribute(this.attributes.widgetName) === 'ezinlinecustomtag';
    },
    isEmbed: function(element) {
        return element.hasClass(this.cssClasses.embedWrapper);
    },
    isEmbedImage: function(element) {
        if (this.isEmbed(element) === false) {
            return false;
        }

        let list = element.find('[' + this.attributes.eZElement + ']');
        if (list.count() === 0) {
            return false;
        }

        return list.getItem(0).hasClass(this.cssClasses.embedImage);
    },
    isCustomStyle: function(element) {
        return element.getAttribute(this.attributes.eZType) === 'style';
    },
    isBlockCustomStyle: function(element) {
        if (this.isCustomStyle(element) === false) {
            return false;
        }
        return element.getName() === 'div';
    },
    isInlineCustomStyle: function(element) {
        if (this.isCustomStyle(element) === false) {
            return false;
        }
        return element.getName() === 'span';
    },
    getCustomTagPathLabel: function(element) {
        let label = this.getElementLabel('custom_tag');
        let list = element.find('[' + this.attributes.eZName + ']');
        if (list.count() > 0) {
            label += ' "' + list.getItem(0).getAttribute(this.attributes.eZName) + '"';
        }
        return label;
    },
    getCustomStylePathLabel: function(element) {
        let label = this.getElementLabel('custom_style');
        if (element.hasAttribute(this.attributes.eZName)) {
            label += ' "' + element.getAttribute(this.attributes.eZName) + '"';
        }
        return label;
    },
    fixElementsForCustomTag: function(element, index) {
        // When custom tag value is focused, there are few extra divs in the path
        // And we need to remove them.
        let indexToCheck = index + 1;
        if (typeof this.elements[indexToCheck] === 'undefined') {
            return;
        }
        if (this.elements[indexToCheck].hasAttribute(this.attributes.eZName)) {
            this.elements[indexToCheck] = null;
        }

        indexToCheck = index + 2;
        if (typeof this.elements[indexToCheck] === 'undefined') {
            return;
        }
        if (this.elements[indexToCheck].hasAttribute(this.attributes.eZElement)) {
            this.elements[indexToCheck] = null;
        }
    },
    getPathItem: function(element, label, dialogs) {
        let item = CKEDITOR.dom.element.createFromHtml(this.itemTemplate.output({label}));

        let that = this;
        item.on('click', function (event) {
            event.data.preventDefault();
            that.editor.elementsPathActiveNode = element;

            if (that.hasCustomAttributesDialog(element, dialogs)) {
                that.editor.execCommand('show-custom-attributes-ui', {
                    element: element,
                    callback: function() {
                        that.focusOnElement(element)
                    }
                });
            } else {
                that.focusOnElement(element);
            }
        });

        let listItem = new CKEDITOR.dom.element('li');
        listItem.append(item);

        return listItem;
    },
    focusOnElement: function(element) {
        // Just focus the widget for embed and image
        if (this.isEmbed(element)) {
            this.focusWidget(element);
            return;
        }

        // For custom tags we need to switch element it its content paragraph
        if (this.isCustomTag(element)) {
            let list = element.find('[data-ezelement="ezcontent"]>p');
            // Just focus the widget if custom tag content is not shown
            if (
                element.getChildCount() === 0
                || element.getChild(0) instanceof CKEDITOR.dom.text
                || element.getChild(0).hasClass(this.cssClasses.customTagContentVisible) === false
                || list.count() === 0
            ) {
                this.focusWidget(element);
                return;
            }

            if (list.count() > 0) {
                element = list.getItem(0);
            }
        }

        // Use first child for quote
        if (element.getName() === 'blockquote') {
            element = element.getChild(0);
        }

        let range = this.editor.createRange();
        range.moveToPosition(element, CKEDITOR.POSITION_AFTER_START);
        this.editor.getSelection().selectRanges([range]);
        let selection = this.editor.getSelectionData();
        this.editor.fire('editorInteraction', {
            nativeEvent: {
                editor: this.editor,
                target: element.$,
            },
            selectionData: {
                element: element,
                text: '',
                region: selection ? selection.region : this.editor.getSelectionRegion()
            }
        });
    },
    hasCustomAttributesDialog: function(element, dialogs) {
        return dialogs.hasOwnProperty(element.getName());
    },
    focusWidget: function(element) {
        this.editor.widgets.getByElement(element).focus();
    }
};

export default UpdateElementsPathCommand;