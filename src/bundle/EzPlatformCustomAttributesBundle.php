<?php

namespace ContextualCode\EzPlatformCustomAttributesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use ContextualCode\EzPlatformCustomAttributesBundle\DependencyInjection\EzPlatformCustomAttributesExtension;

class EzPlatformCustomAttributesBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension(): EzPlatformCustomAttributesExtension
    {
        if (null === $this->extension) {
            $this->extension = new EzPlatformCustomAttributesExtension();
        }

        return $this->extension;
    }
}
