import UpdateElementsPathCommand from './base/elements-path';
import ShowCustomAttributesDialogs from './base/dialogs';

(function(global) {
    if (CKEDITOR.plugins.get('custom-attributes')) {
        return;
    }

    const InstallElementsPath = function(editor) {
        const wrapper = new CKEDITOR.dom.element('div');
        wrapper.addClass('elements-path-wrapper');
        editor.container.getParent().append(wrapper);

        const elementsPath = new CKEDITOR.dom.element('ul');
        wrapper.append(elementsPath);

        editor.elementsPathContainer = elementsPath;
    };

    const DialogPrefix = 'custom-attributes-';
    const ShowCustomAttributesDialogCommand = {
        exec: function(editor, options) {
            const dialogName = DialogPrefix + options.element.getName();
            editor.openDialog(dialogName, function() {
                if (options.callback) {
                    options.callback();
                }
            });
        },
        canUndo: false,
        editorFocus: 1,
    };

    CKEDITOR.plugins.add('custom-attributes', {
        init: (editor) => {
            const DialogDefinitions = ShowCustomAttributesDialogs.getDefinitions(
                editor,
                global.eZ.adminUiConfig.richTextCustomClasses,
                global.eZ.adminUiConfig.richTextCustomAttributes
            );
            for (let [key, definition] of Object.entries(DialogDefinitions)) {
                CKEDITOR.dialog.add(DialogPrefix + key, function() {
                    return definition;
                });
            }
            editor.addCommand('update-elements-path', UpdateElementsPathCommand);
            editor.addCommand('show-custom-attributes-ui', ShowCustomAttributesDialogCommand);

            editor.on('contentDom', (event) => {
                InstallElementsPath(event.editor);
            });
            editor.on('selectionChange', (event) => {
                event.editor.execCommand('update-elements-path', {
                    path: event.data.path.elements,
                    dialogs: DialogDefinitions
                });
            });
        },
    });
})(window);
