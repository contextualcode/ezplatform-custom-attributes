<?php

namespace ContextualCode\EzPlatformCustomAttributesBundle\DependencyInjection;

use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\Configuration\SiteAccessAware\Configuration as SiteAccessConfiguration;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration extends SiteAccessConfiguration
{
    const CUSTOM_ATTRIBUTE_TYPES = ['text', 'select', 'checkbox', 'textarea'];

    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ezrichtext_extension');

        $sections = $rootNode->children();
        $this
            ->addCustomClassesSection($sections);
        $this
            ->addCustomAttributesSection($sections);
        $this
            ->addEmbedViewsSection($sections)
            ->end();

        return $treeBuilder;
    }

    /**
     * Define RichText Custom Classes Semantic Configuration.
     *
     * The configuration is available at:
     * <code>
     * ezrichtext_extension:
     *     custom_classes:
     *         p: [class-p-1, class-p-2]
     *         tr: [class-tr-1, class-tr-2, class-tr-3]
     * </code>
     *
     * @param NodeBuilder $ezRichTextNode
     *
     * @return NodeBuilder
     */
    protected function addCustomClassesSection(NodeBuilder $ezRichTextNode): NodeBuilder
    {
        return $ezRichTextNode
            ->arrayNode('custom_classes')
                ->arrayPrototype()
                    ->example(['class-1', 'class-2'])
                    ->prototype('scalar')->end()
                ->end()
            ->end();
    }

    /**
     * Define RichText Custom Attributes Semantic Configuration.
     *
     * The configuration is available at:
     * <code>
     * ezrichtext_extension:
     *     custom_attributes:
     *         p:
     *             text-attr:
     *                 type: text
     *                 default: 'Default value'
     *             select-attr:
     *                 type: select
     *                 options: [first, second, third]
     *             checkbox-attr:
     *                 type: checkbox
     *             textarea-attr:
     *                 type: textarea
     *         table:
     *             select-attr:
     *                 type: select
     *                 options: [first, second, third]
     *             checkbox-attr:
     *                 type: checkbox
     * </code>
     *
     * @param NodeBuilder $ezRichTextNode
     *
     * @return NodeBuilder
     */
    protected function addCustomAttributesSection(NodeBuilder $ezRichTextNode): NodeBuilder
    {
        return $ezRichTextNode
            ->arrayNode('custom_attributes')
                ->normalizeKeys(false)
                ->arrayPrototype()
                    ->normalizeKeys(false)
                    ->arrayPrototype()
                        ->children()
                            ->enumNode('type')
                                ->isRequired()
                                ->values(static::CUSTOM_ATTRIBUTE_TYPES)
                            ->end()
                            ->scalarNode('default')
                                ->defaultNull()
                            ->end()
                            ->arrayNode('options')
                                ->example(['options_1', 'option_2'])
                                ->prototype('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * Define RichText Embed Views Semantic Configuration.
     *
     * The configuration is available at:
     * <code>
     * ezrichtext_extension:
     *     embed_views: [view-1, view-2, view-3]
     * </code>
     *
     * @param NodeBuilder $ezRichTextNode
     *
     * @return NodeBuilder
     */
    protected function addEmbedViewsSection(NodeBuilder $ezRichTextNode): NodeBuilder
    {
        return $ezRichTextNode
            ->arrayNode('embed_views')
                ->example(['view-1', 'view-2', 'view-3'])
                ->prototype('scalar')->end()
            ->end();
    }
}
